# SPDX-FileCopyrightText: 2020, 2021 Romain Vigier <contact AT romainvigier.fr>
# SPDX-License-Identifier: GPL-3.0-or-later

variables:
  PACKAGE_NAME: nightthemeswitcher.zip
  PACKAGE_REGISTRY_URL: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/nightthemeswitcher

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "web"

stages:
  - check
  - build
  - deploy

.python:
  image: python:latest
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
  cache:
    key: python
    paths:
      - .cache/pip
      - venv/
  before_script:
    - python -V
    - pip install virtualenv
    - virtualenv venv
    - source venv/bin/activate

.node:
  image: node:latest
  cache:
    key: node
    paths:
      - node_modules
  before_script:
    - npm install --save-dev

lint:
  extends: .node
  stage: check
  interruptible: true
  rules:
    - changes:
      - src/**/*.js
      - tests/**/*.js
  script:
    - npm run test-lint

variants:
  extends: .node
  stage: check
  interruptible: true
  rules:
    - changes:
      - src/modules/GtkVariants.js
      - src/modules/ShellVariants.js
      - tests/**/*.js
  script:
    - npm run test-variants

reuse:
  extends: .python
  stage: check
  interruptible: true
  script:
    - pip install reuse
    - reuse lint

extension:
  stage: build
  interruptible: true
  needs:
    - job: lint
      optional: true
    - job: variants
      optional: true
    - reuse
  image: registry.gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension:latest
  script:
    - make build
  artifacts:
    name: $CI_COMMIT_REF_NAME
    expose_as: Extension
    paths:
      - build/nightthemeswitcher@romainvigier.fr.shell-extension.zip
    expire_in: 1 day

package:
  stage: deploy
  needs:
    - job: extension
      artifacts: true
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl \
        --header "JOB-TOKEN: $CI_JOB_TOKEN" \
        --upload-file build/nightthemeswitcher@romainvigier.fr.shell-extension.zip \
        "${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${PACKAGE_NAME}"

release-notes:
  stage: deploy
  needs:
    - reuse
  image: rust:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - cargo install parse-changelog
    - parse-changelog --version-format="^\d+$" CHANGELOG.md $CI_COMMIT_TAG > release-notes.md
  artifacts:
    paths:
      - release-notes.md
    expire_in: 1 day

release:
  stage: deploy
  needs:
    - package
    - job: release-notes
      artifacts: true
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Creating release for version $CI_COMMIT_TAG"
  release:
    name: Version $CI_COMMIT_TAG
    tag_name: $CI_COMMIT_TAG
    description: release-notes.md
    assets:
      links:
        - name: Night Theme Switcher v${CI_COMMIT_TAG}
          url: ${PACKAGE_REGISTRY_URL}/${CI_COMMIT_TAG}/${PACKAGE_NAME}
          filepath: /${PACKAGE_NAME}
          link_type: package

pages:
  stage: deploy
  needs: []
  image: registry.gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension:latest
  script:
    - cp -r website public
    - find public -type f -regex '.*\.\(|html\|css\|svg\|png\|woff\|woff2\)$' -exec gzip -f -k {} \;
    - find public -type f -regex '.*\.\(|html\|css\|svg\|png\|woff\|woff2\)$' -exec brotli -f -k {} \;
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
      - website/**/*
  artifacts:
    paths:
      - public
    expire_in: 1 day
