# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Night Theme Switcher package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Night Theme Switcher\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-25 14:58+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/modules/GtkThemer.js:184
#, javascript-format
msgid ""
"Unable to automatically detect the day and night variants for the \"%s\" GTK "
"theme. Please manually choose them in the extension's preferences."
msgstr ""

#: src/modules/ShellThemer.js:196
#, javascript-format
msgid ""
"Unable to automatically detect the day and night variants for the \"%s\" "
"GNOME Shell theme. Please manually choose them in the extension's "
"preferences."
msgstr ""

#: src/modules/TimerOndemand.js:246
msgid "Switch to night theme"
msgstr ""

#: src/modules/TimerOndemand.js:246
msgid "Switch to day theme"
msgstr ""

#: src/preferences/Schedule.js:57 src/preferences/ui/Schedule.ui:62
msgid "Night Light"
msgstr ""

#: src/preferences/Schedule.js:67
msgid "Location Services"
msgstr ""

#: src/preferences/Schedule.js:80
msgid "Manual schedule"
msgstr ""

#: src/preferences/Schedule.js:81 src/preferences/ui/Schedule.ui:135
msgid "On-demand"
msgstr ""

#: src/preferences/ShellPreferences.js:55
msgid "Default"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:10
msgid "Settings version"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:11
msgid "The current extension settings version"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:17
msgid "Switch GTK variants"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:18
msgid "Enable GTK variants switching"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:22
msgid "Day GTK theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:23
msgid "The GTK theme to use during daytime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:27
msgid "Night GTK theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:28
msgid "The GTK theme to use during nighttime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:32
msgid "Use manual GTK variants"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:33
msgid "Disable automatic GTK theme variants detection"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:39
msgid "Switch shell variants"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:40
msgid "Enable shell variants switching"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:44
msgid "Day shell theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:45
msgid "The shell theme to use during daytime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:49
msgid "Night shell theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:50
msgid "The shell theme to use during nighttime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:54
msgid "Use manual shell variants"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:55
msgid "Disable automatic shell theme variants detection"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:61
msgid "Switch icon variants"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:62
msgid "Enable icon variants switching"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:66
msgid "Day icon theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:67
msgid "The icon theme to use during daytime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:71
msgid "Night icon theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:72
msgid "The icon theme to use during nighttime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:78
msgid "Switch cursor variants"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:79
msgid "Enable cursor variants switching"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:83
msgid "Day cursor theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:84
msgid "The cursor theme to use during daytime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:88
msgid "Night cursor theme"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:89
msgid "The cursor theme to use during nighttime"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:95
msgid "Enable commands"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:96
msgid "Commands will be spawned on time change"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:100
msgid "Sunrise command"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:101
msgid "The command to spawn at sunrise"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:105
msgid "Sunset command"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:106
msgid "The command to spawn at sunset"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:112
msgid "Enable backgrounds"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:113
msgid "Background will be changed on time change"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:117
#: src/preferences/ui/BackgroundsPreferences.ui:39
msgid "Day background"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:118
msgid "Path to the day background"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:122
#: src/preferences/ui/BackgroundsPreferences.ui:47
msgid "Night background"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:123
msgid "Path to the night background"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:135
#: src/preferences/ui/Schedule.ui:36
msgid "Time source"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:136
msgid "The source used to check current time"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:140
msgid "Follow Night Light \"Disable until tomorrow\""
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:141
msgid "Switch back to day time when Night Light is temporarily disabled"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:145
msgid "Always enable the on-demand timer"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:146
msgid "The on-demand timer will always be enabled alongside other timers"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:154
msgid "On-demand time"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:155
msgid "The current time used in on-demand mode"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:159
msgid "Key combination to toggle time"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:160
msgid "The key combination that will toggle time in on-demand mode"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:169
msgid "On-demand button placement"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:170
msgid "Where the on-demand button will be placed"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:174
msgid "Use manual time source"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:175
msgid "Disable automatic time source detection"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:179
msgid "Sunrise time"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:180
msgid "When the day starts"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:184
msgid "Sunset time"
msgstr ""

#: src/schemas/org.gnome.shell.extensions.nightthemeswitcher.gschema.xml:185
msgid "When the day ends"
msgstr ""

#: src/preferences/ui/BackgroundButton.ui:15
#: src/preferences/ui/ShortcutButton.ui:15
msgid "Choose…"
msgstr ""

#: src/preferences/ui/BackgroundButton.ui:32
msgid "Change background"
msgstr ""

#: src/preferences/ui/BackgroundButton.ui:44
#: src/preferences/ui/BackgroundButton.ui:46
#: src/preferences/ui/ClearableEntry.ui:10
#: src/preferences/ui/ShortcutButton.ui:43
#: src/preferences/ui/ShortcutButton.ui:45
msgid "Clear"
msgstr ""

#: src/preferences/ui/BackgroundButton.ui:61
msgid "Select your background image"
msgstr ""

#: src/preferences/ui/BackgroundsPreferences.ui:15
msgid "Switch backgrounds"
msgstr ""

#: src/preferences/ui/Commands.ui:31
msgid "Run commands"
msgstr ""

#: src/preferences/ui/Commands.ui:55 src/preferences/ui/Schedule.ui:107
msgid "Sunrise"
msgstr ""

#. Don't translate the `notify-send` command.
#: src/preferences/ui/Commands.ui:58
msgid "notify-send \"Hello sunshine!\""
msgstr ""

#: src/preferences/ui/Commands.ui:67 src/preferences/ui/Schedule.ui:115
msgid "Sunset"
msgstr ""

#. Don't translate the `notify-send` command.
#: src/preferences/ui/Commands.ui:70
msgid "notify-send \"Hello moonshine!\""
msgstr ""

#: src/preferences/ui/CursorPreferences.ui:15
msgid "Switch cursor theme variants"
msgstr ""

#: src/preferences/ui/CursorPreferences.ui:39
#: src/preferences/ui/GtkPreferences.ui:48
#: src/preferences/ui/IconPreferences.ui:39
#: src/preferences/ui/ShellPreferences.ui:48
msgid "Day variant"
msgstr ""

#: src/preferences/ui/CursorPreferences.ui:49
#: src/preferences/ui/GtkPreferences.ui:59
#: src/preferences/ui/IconPreferences.ui:49
#: src/preferences/ui/ShellPreferences.ui:59
msgid "Night variant"
msgstr ""

#: src/preferences/ui/GtkPreferences.ui:15
msgid "Switch GTK theme variants"
msgstr ""

#: src/preferences/ui/GtkPreferences.ui:39
#: src/preferences/ui/ShellPreferences.ui:39
msgid "Manual variants"
msgstr ""

#: src/preferences/ui/GtkPreferences.ui:40
msgid ""
"You can manually set variants if the extension cannot automatically detect "
"the day and night variants of your GTK theme. Please <a href=\"https://"
"gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension/-/issues\">submit "
"a request</a> to get your theme supported."
msgstr ""

#: src/preferences/ui/Headerbar.ui:12 src/preferences/ui/Headerbar.ui:24
msgid "Support us"
msgstr ""

#: src/preferences/ui/Headerbar.ui:48
msgid "View code on GitLab"
msgstr ""

#: src/preferences/ui/Headerbar.ui:71
msgid "Translate on Weblate"
msgstr ""

#: src/preferences/ui/Headerbar.ui:94
msgid "Donate on Liberapay"
msgstr ""

#: src/preferences/ui/IconPreferences.ui:15
msgid "Switch icon theme variants"
msgstr ""

#: src/preferences/ui/Preferences.ui:14
msgid "Schedule"
msgstr ""

#: src/preferences/ui/Preferences.ui:24
msgid "Appearance"
msgstr ""

#: src/preferences/ui/Preferences.ui:34
msgid "Commands"
msgstr ""

#: src/preferences/ui/Schedule.ui:27
msgid "Manual time source"
msgstr ""

#: src/preferences/ui/Schedule.ui:28
msgid ""
"The extension will try to use Night Light or Location Services to "
"automatically set your current sunrise and sunset times if they are enabled. "
"If you prefer, you can manually choose a time source."
msgstr ""

#: src/preferences/ui/Schedule.ui:45
msgid "Always show on-demand controls"
msgstr ""

#: src/preferences/ui/Schedule.ui:46
msgid "Allows you to override the current time when using a schedule."
msgstr ""

#: src/preferences/ui/Schedule.ui:74
msgid "Follow <i>Disable until tomorrow</i>"
msgstr ""

#: src/preferences/ui/Schedule.ui:75
msgid ""
"When Night Light is temporarily disabled, the extension will switch to day "
"variants."
msgstr ""

#: src/preferences/ui/Schedule.ui:95
msgid "Manual schedule times"
msgstr ""

#: src/preferences/ui/Schedule.ui:147
msgid "Keyboard shortcut"
msgstr ""

#: src/preferences/ui/Schedule.ui:155
msgid "Button location"
msgstr ""

#: src/preferences/ui/Schedule.ui:160
msgid "None"
msgstr ""

#: src/preferences/ui/Schedule.ui:161
msgid "Top bar"
msgstr ""

#: src/preferences/ui/Schedule.ui:162
msgid "System menu"
msgstr ""

#: src/preferences/ui/ShellPreferences.ui:15
msgid "Switch Shell theme variants"
msgstr ""

#: src/preferences/ui/ShellPreferences.ui:40
msgid ""
"You can manually set variants if the extension cannot automatically detect "
"the day and night variants of your GNOME Shell theme. Please <a href="
"\"https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension/-/issues"
"\">submit a request</a> to get your theme supported."
msgstr ""

#: src/preferences/ui/ShortcutButton.ui:32
msgid "Change keyboard shortcut"
msgstr ""

#: src/preferences/ui/ShortcutButton.ui:100
msgid "Press your keyboard shortcut…"
msgstr ""

#. Time separator (eg. 08:27)
#: src/preferences/ui/TimeChooser.ui:23
msgid ":"
msgstr ""
